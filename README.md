# Projetc Folder Construction

```
   .
    ├── public                   # Compiled folder    
    │   ├── index.html    
    │   └── ...           
    ├── src                    # Source folder 
    │   ├── actions            # Redux Action folder 
    │   ├── components         # React Component folder 
    │   ├── constants          # Store Constants folder  
    │   ├── reducers           # Redux Reducer folder 
    │   ├── routers            # Router folder 
    │   ├── store              # Redux Store folder 
    │   ├── index.js           
    │   └── serviceWorker.js                 
    ├── config-overrides.js      # Override Configuration file 
    ├── package.json             # Package Management file 
    └── README.md
```
- package.json: This File has the list of node dependencies which are needed.
- public/index.html: When the application starts this is the first page that is loaded. This will be the only html file in the entire application since React is generally Written using JSX.
- src/index.js: This is the javascript file corresponding to index.html. 

Since Redux is just a data store library, it has no direct opinion on how your project should be structured. I use :
Rails-style: separate folders for “actions”, “constants”, “reducers”, “containers”, and “components”

# Redux
 
 stateful React component is a JavaScript ES6 class and its carries its own state
 In a React component the state holds up data and the component might render such data to the user. The state could also change in response to actions and events: in React you can update the local component’s state with setState.
 any event in your apploicatiin can be can be store as state even just clicke the buttom and opening a mudule. in order keeping track of state we can use Redux library 

### Store 
A [store](https://redux.js.org/api/store) holds the whole state tree of your application. The only way to change the state inside it is to dispatch an action on it.
pass your root reducing function to createStore.
 As your app grows, instead of adding stores, you split the root reducer into smaller reducers independently operating on the different parts of the state tree. You can use a helper like combineReducers to combine them
- getState()
Returns the current state tree of your application. It is equal to the last value returned by the store's reducer.
- dispatch(action)
Dispatches an action. This is the only way to trigger a state change.
[more info](https://redux.js.org/api/store)

### Action
[Actions](https://redux.js.org/basics/actions) are payloads of information that send data from your application to your store. They are the only source of information for the store. You send them to the store using store.dispatch().
Actions are plain JavaScript objects. Actions must have a type property that indicates the type of action being performed
- Action Creators
Action creators are exactly that—functions that create actions. It's easy to conflate the terms “action” and “action creator”, so do your best to use the proper term.
In Redux to initiate a dispatch, pass the Action creators result to the dispatch() function

- [Async Actions](https://redux.js.org/advanced/async-actions)

we need synchronous action creators wehn we are handeling API calls
When you call an asynchronous API, there are two crucial moments in time: the moment you start the call, and the moment when you receive an answer.

Each of these two moments usually require a change in the application state; to do that, you need to dispatch normal actions that will be processed by reducers synchronously. Usually, for any API request you'll want to dispatch at least three different kinds of actions:

- An action informing the reducers that the request began.
- An action informing the reducers that the request finished successfully.
- An action informing the reducers that the request failed.

The standard way to use the synchronous action creators network requests with Redux is to use the [Redux Thunk middleware](https://github.com/reduxjs/redux-thunk). It comes in a separate package called redux-thunk. We'll explain how middleware works in general later; for now, there is just one important thing you need to know: by using this specific middleware, an action creator can return a function instead of an action object. This way, the action creator becomes a thunk.



### State Shape

{
    calculateDate:{
        add:{
            loading: false,
            result: null,
            error: null,
        },
        sub:{
            loading: false,
            result: null,
            error: null,
        } 
    }, 
}


### Reducer
Reducers specify how the application's state changes in response to actions sent to the store
actions only describe what happened, but don't describe how the application's state changes.
The reducer is a pure function that takes the previous state and an action, and returns the next state.

Handling Actions
It's called a reducer because it's the type of function you would pass to Array.prototype.reduce(reducer, ?initialValue). It's very important that the reducer stays pure. Things you should never do inside a reducer:



### Ant Design

An enterprise-class UI design language and React implementation with a set of high-quality React components, one of best React UI library for enterprises.
using the ant design leyout and from in this application 
https://ant.design/docs/react/introduce
https://ant.design/docs/react/use-with-create-react-app


### Eslint 
  
tool for identifying and reporting on patterns in JavaScript. Maintain your code quality with ease. Useing airbnb extention for React app 

Use custom ESLint config in create-react-app

http://rahulgaba.com/front-end/2019/02/17/Use-custom-eslint-config-in-create-react-app-using-three-simple-steps-No-external-dependencies.html















