import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers } from 'redux';
import calculateNumber from '../reducers/calculateNumbers';
import getDate from '../reducers/getDate';

const rootReducer = combineReducers({
  calculateNumber,
  getDate,

})
const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware),
);

export default store;
