import React, { Component } from 'react';
import SubForm from './SubForm';
import SunInfo from './SubInfo'
import Result from '../result';
import '../../style/page.css'

class Sub extends Component {
  render() {
    /*     const { dispatch } = this.props
    dispatch(getDateAction.getDate()) */
    return (
        <div id="page">
            <SunInfo />
            <SubForm />
            <Result />
        </div>
    );
  }
}

export default Sub;
