import React from 'react';

import '../../style/info.css'

const SubInfo = () => (

    <div id="info">
        <div id="subLogo" />
        <h2>Subtract from Date</h2>
        <p>
Select a number of days to subtract from the selected date.
The resulting date will show on the history section of both pages.
        </p>
    </div>

);
export default SubInfo;
