import React from 'react';
import '../../style/info.css'

const AddInfo = () => (

    <div id="info">
        <div id="addLogo" />
        <h2>Add to Date</h2>
        <p>
Select a number of days to add to the selected date.
The resulting date will show on the history section of both pages.
        </p>
    </div>

);
export default AddInfo;
