import React, { Component } from 'react';
import AddForm from './AddForm';
import AddInfo from './AddInfo'
import Result from '../result';
import '../../style/page.css'

class Add extends Component {
  render() {
    return (
        <div id="page">
            <AddInfo />
            <AddForm />
            <Result />
        </div>
    );
  }
}

export default Add;
