import React, { Component } from 'react';
import { connect } from 'react-redux';
import { calculateNumberAction } from '../../actions';
import Add_Sub_Form from '../form/Add-Sub-Form'
import '../../style/form.css'

function mapStateToProps(state) {
  const mystate = state.getDate
  return {
    result: mystate.result,
    error: mystate.error,
  };
}
class ConnectedForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      num: '',
      date: '',
      error: '',
      isValid: true,

    };

    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleDatePickerChange = this.handleDatePickerChange.bind(this);
    this.handleAddSubmit = this.handleAddSubmit.bind(this);

  }

  handleDatePickerChange = (_, dateString) => {
    console.log(dateString)
    this.setState({ date: dateString })
  }

  handleChangeInput(event) {
    this.setState({ [ event.target.name ]: event.target.value });
  }

  handleAddSubmit(event) {
    event.preventDefault();
    const { dispatch } = this.props
    const { num, date } = this.state

    if ( num == "" ){
      this.setState({ 
        isValid: false ,
        error:"Please enter number of days you wish to calculate"
      } );
      return
    }else if(date =="") {
      this.setState({ 
        isValid: false ,
        error:"Please enter the date you wish to calculate"
      } );
      return
    }
    const number = parseInt( num, 10)
    dispatch(calculateNumberAction.add(date, number))
  }

 

  render() {
    return (
        <div>
            <div id="addForm">
                <Add_Sub_Form
                  isValid={ this.state.isValid }
                  error={ this.state.error }
                  onChangeDataPicker={ this.handleDatePickerChange }
                  onChange={ this.handleChangeInput }
                  onClick={ this.handleAddSubmit }
                  label="Days to add"
                  value="add"
                  name="add"
                  num={ this.state.num }
                />
            </div>
        </div>
    );
  }
}

const AddForm = connect(mapStateToProps)(ConnectedForm);

export default AddForm;
