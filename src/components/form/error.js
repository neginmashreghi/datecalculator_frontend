import React, { Component } from 'react';
import '../../style/form.css'

class Error extends Component {
  render() {
    return (
        <div id="error">
            <div id="errorLogo" />
            <div id="errorMsg">{this.props.error}</div>
        </div>
    )
  }
}
export default Error;
