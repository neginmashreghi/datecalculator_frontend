import React, { Component } from 'react';
import {
  DatePicker, Form, Button, Input,
} from 'antd';

import Error from './error'


class Add_Sub_Form extends Component {
  reset = (e) => {
    e.preventDefault();
    this.props.form.resetFields()
  }
 
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        span: 12,
      },
      wrapperCol: {
        span: 24,
      },
    };
    let ErrorDiv;
    if (this.props.isValid == false) {
      ErrorDiv = <Error error={ this.props.error } />
    }
    const dateFormat = 'YYYY/MM/DD';
   
    return (

        <Form layout="inline" { ...formItemLayout }>
            <div id="input-label">
                {ErrorDiv}
                <Form.Item label="Date">
                    {getFieldDecorator('dataPicker', {
                
                    })( <DatePicker
                      format={ dateFormat }
                      placeholder="Pick a Date"
                      onChange={ this.props.onChangeDataPicker }
                    />)
                    }

                </Form.Item>
                <Form.Item label={ this.props.label }>
                    {getFieldDecorator('days', {
                    })(<Input
                      placeholder="Pick Number of Days"
                      name="num"
                      value={ this.props.num }
                      onChange={ this.props.onChange }
                    />)
                    }
                </Form.Item>
            </div>
            <div id="buttons">
                <Form.Item>
                    <Button id="btn" type="primary" value={ this.props.value } onClick={ this.props.onClick }>{ this.props.name }</Button>
                    <Button id="btn" value="reset" onClick={ this.reset }>reset</Button>
                </Form.Item>
            </div>

        </Form>
    );
  }
}

const WrappedAdd_Sub_Form = Form.create()(Add_Sub_Form);
export default WrappedAdd_Sub_Form;
