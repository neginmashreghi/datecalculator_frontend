/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { connect } from 'react-redux';

import './result.css'

class ConnectedResult extends React.PureComponent {
  render() {
    const { history } = this.props

    return (
        <div id="result">
            <p>Add/Subtract Result History</p>
            <hr />
            <div>
                {Object.keys(history).map((result, index) => (
                    <p key={ index }>
                        {history[ result ]}
                    </p>
                ))}
            </div>
        </div>
    );
  }
}

function mapStateToProps(state) {
  const mystate = state.calculateNumber
  return {
    history: mystate.history,

  };
}

const Result = connect(mapStateToProps)(ConnectedResult)

export default Result;
