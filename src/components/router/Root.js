import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';

import { connect } from 'react-redux';
import Add from '../add';
import Sub from '../subtarct';


import { getDateAction } from '../../actions';

import './root.css'

const {
  Header, Content, Sider, Footer,
} = Layout;
const { SubMenu } = Menu;

class RoutersComponent extends Component {
    state = {
      collapsed: false,
    };

    onCollapse = (collapsed) => {
      this.setState({ collapsed });
    }

    toggle = () => {
      const { collapsed } = this.state
      this.setState({
        collapsed: !collapsed,
      });
    }

    render() {
      const { dispatch } = this.props
      dispatch(getDateAction.getDate())
      const { collapsed } = this.state
      return (
          <Router>
              <Layout style={ { minHeight: '100vh' } }>
                  <Sider

                    trigger={ null }
                    collapsible
                    collapsed={ collapsed }
                    onCollapse={ this.onCollapse }
                  >
                      <div className="logo">
                         
                      </div> 

                      <Menu theme="dark" defaultSelectedKeys={ [ '1' ] } mode="inline">   
                        <Menu.Item key="2">
                            <Icon type="plus" />
                            <span>Add</span>
                            <Link to="/" />
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="minus" />
                            <span>Subtract</span>
                            <Link to="/subtract" />
                        </Menu.Item>
                      </Menu>
                  </Sider>
                  <Layout>

                      <Header style={ { background: '#fff', padding: 0, paddingLeft: 16 } }>
                          <Icon
                            className="trigger"
                            type={ collapsed ? 'menu-unfold' : 'menu-fold' }
                            style={ { cursor: 'pointer' } }
                            onClick={ this.toggle }
                          />
                      </Header>

                      <Content style={ {
                        margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
                      } }
                      >

                          <Route exact path="/" component={ Add } />
                          <Route path="/subtract" component={ Sub } />
                      </Content>
                      
                      <Footer>
                      Copyright 2019 Develop & Conquer Inc.
                      </Footer>

                  </Layout>

              </Layout>
          </Router>
      );
    }
}

const Routers = connect()(RoutersComponent)
export default Routers;
