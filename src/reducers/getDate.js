/*
  getDate reducer handle getDate API Actin

*/
import {
  GET_DATE_SUCCESS,
  GET_DATE_FAILURE,
  GET_DATE_BEGIN,
} from '../constants/actionTypes';

const initialState = {
  loading: false,
  result: null,
  error: null,
};

function getDate(state = initialState, action) {
  switch (action.type) {
    case GET_DATE_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case GET_DATE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        result: action.response,
      };
    case GET_DATE_FAILURE:
      return {
        ...state,
        result: null,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export default getDate;
