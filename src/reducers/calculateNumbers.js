/*
  calculateNumber reducer handle add and sub API Actin
*/
import {
  ADD_NUMBER_SUCCESS,
  ADD_NUMBER_FAILURE,
  ADD_NUMBER_BEGIN,
  SUB_NUMBER_SUCCESS,
  SUB_NUMBER_FAILURE,
  SUB_NUMBER_BEGIN,
} from '../constants/actionTypes';

const initialState = {

  loading: false,
  result: null,
  error: null,
  history: [],

};

function calculateNumber(state = initialState, action) {
  switch (action.type) {
    case ADD_NUMBER_BEGIN:
    case SUB_NUMBER_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case ADD_NUMBER_SUCCESS:
    case SUB_NUMBER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        result: action.response,
        history: [ ...state.history, action.response ],
      };
    case ADD_NUMBER_FAILURE:
    case SUB_NUMBER_FAILURE:
      return {
        ...state,
        result: null,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export default calculateNumber;
