/* eslint-disable func-names */
import axios from 'axios';
import {
  GET_DATE_SUCCESS,
  GET_DATE_FAILURE,
  GET_DATE_BEGIN,
} from '../constants/actionTypes';

// eslint-disable-next-line import/prefer-default-export
export const getDateAction = {
  getDate,
}

// action creator
function getDateRequest() {
  return {
    type: GET_DATE_BEGIN,
  }
}

function getDateResponse(response) {
  return {
    type: GET_DATE_SUCCESS,
    response,
  }
}

function getDateError(error) {
  return {
    type: GET_DATE_FAILURE,
    error,

  }
}

// action crea
function getDate() {
  return function (dispatch) {
    dispatch(getDateRequest())

    axios
      .post('http://localhost:8080/today')
      .then((res) => {
        dispatch(getDateResponse(res.data));
      })
      .catch((err) => {
        dispatch(getDateError(err.message));
      });
  }
}
