/* eslint-disable func-names */
import axios from 'axios';
import {
  ADD_NUMBER_SUCCESS,
  ADD_NUMBER_FAILURE,
  ADD_NUMBER_BEGIN,
  SUB_NUMBER_SUCCESS,
  SUB_NUMBER_FAILURE,
  SUB_NUMBER_BEGIN,

} from '../constants/actionTypes';

// eslint-disable-next-line import/prefer-default-export
export const calculateNumberAction = {
  add,
  sub,
}

function addNumberRequest() {
  return {
    type: ADD_NUMBER_BEGIN,
  }
}

function addNumberResponse(response) {
  return {
    type: ADD_NUMBER_SUCCESS,
    response,

  }
}

function addNumberError(error) {
  return {
    type: ADD_NUMBER_FAILURE,
    error,
  }
}

function add( ranDate, num ) {
  return function (dispatch) {
    dispatch(addNumberRequest())
    axios
      .post('http://localhost:8080/add',
        {
          ranDate,
          num,
        })
      .then((res) => {
        dispatch(addNumberResponse(res.data));
        // dispatch(resultHistoryAction.createResultHistory(res.data))
      })
      .catch((err) => {
        dispatch(addNumberError(err.message));
      });
  }
}

// ////////////////////////////////////////////////////////

function subNumberRequest() {
  return {
    type: SUB_NUMBER_BEGIN,
  }
}

function subNumberResponse(response) {
  return {
    type: SUB_NUMBER_SUCCESS,
    response,

  }
}

function subNumberError(error) {
  return {
    type: SUB_NUMBER_FAILURE,
    error,

  }
}

function sub( ranDate, num ) {
  return function (dispatch) {
    dispatch(subNumberRequest())

    axios
      .post('http://localhost:8080/sub',
        {
          ranDate,
          num,
        })
      .then((res) => {
        dispatch(subNumberResponse(res.data));
        // dispatch(resultHistoryAction.createResultHistory(res.data))
      })
      .catch((err) => {
        dispatch(subNumberError(err.message));
      });
  }
}
